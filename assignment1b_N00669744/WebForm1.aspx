﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="assignment1b_N00669744.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GroupUp</title>
    <script type="text/javascript" lang="javascript"> </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="register" runat="server">
            <h1>Looking for Group: Online Play </h1>

            <label runat="server" for="teamName">Team Name: </label><asp:TextBox runat="server" ID="teamName" placeholder="Enter Team Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter a Team Name" ControlToValidate="teamName" ID="nameValidation"></asp:RequiredFieldValidator>
            <br />

            <label runat="server" for="gameName">Game Name: </label><asp:TextBox runat="server" ID="gameName" placeholder="Enter Game Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter a Game Name" ControlToValidate="gameName" ID="gameValidation"></asp:RequiredFieldValidator>
            <br />
            <br />
            <label runat="server" for="playerNum"># of Teammates Needed:</label>
              <asp:DropDownList runat="server" ID="playerNum">
                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                <asp:ListItem Value="5" Text="5"></asp:ListItem>
            </asp:DropDownList> 
            <br />

           <label for="availabilityGroup">Day(s) Available: </label>
            <div id="days_container" runat="server">
                <asp:CheckBox runat="server" ID="SUN" Text="Sunday" GroupName="availabilityGroup" />
                <asp:CheckBox runat="server" ID="MON" Text="Monday" GroupName="availabilityGroup"/>
                <asp:CheckBox runat="server" ID="TUES" Text="Tuesday" GroupName="availabilityGroup"/>
                <asp:CheckBox runat="server" ID="WED" Text="Wednesday" GroupName="availabilityGroup"/>
                <asp:CheckBox runat="server" ID="THU" Text="Thursday" GroupName="availabilityGroup"/>
                <asp:CheckBox runat="server" ID="FRI" Text="Friday" GroupName="availabilityGroup"/>
                <asp:CheckBox runat="server" ID="SAT" Text="Saturday" GroupName="availabilityGroup"/>
            </div> 
            <br />
            
            <label for="roles_container">Looking for role(s): </label>
                 <div id="roles_container" runat="server">
                    <asp:CheckBox runat="server" ID="DPS" Text="DPS" GroupName="rolesGroup" />
                    <asp:CheckBox runat="server" ID="TANK" Text="TANK" GroupName="rolesGroup"/>
                    <asp:CheckBox runat="server" ID="SUPPORT" Text="SUPPORT" GroupName="rolesGroup"/>
                    <asp:CheckBox runat="server" ID="UTILITY" Text="UTILITY" GroupName="rolesGroup"/>
                </div>
            <br />

            <label runat="server" for="console">Gaming Platform: </label>
            <asp:RadioButtonList ID="console" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" onClick="passVisible">
                <asp:ListItem Selected="True" Value="PC">PC</asp:ListItem>
                <asp:ListItem Value="PS4">PS4</asp:ListItem>
                <asp:ListItem Value="XBOX1">XBOX 1</asp:ListItem>
             </asp:RadioButtonList>
            <br />
            <br />
       
            <label runat="server" for="emailInput">Email: </label><asp:TextBox runat="server" ID="emailInput" TextMode="Email"></asp:TextBox>
                <asp:RegularExpressionValidator ID="emailCheck" runat="server" ControlToValidate="emailInput" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter an Email!" ControlToValidate="emailInput"></asp:RequiredFieldValidator>
            <br />
            <br />

            <label runat="server" for="group">Group Type: </label>
            <div runat="server" id="group">
                <asp:RadioButton runat="server" Text="Public" Checked="true" GroupName="groupType" onchange="passField.style.visibility = 'hidden';" Enabled="true"></asp:RadioButton>
                <asp:RadioButton runat="server" ID="privateGroup" Text="Private" GroupName="groupType" onchange="passField.style.visibility = 'visible';"></asp:RadioButton>
            </div>
             
            <div id="passField" style="visibility:hidden" runat="server">
            <label runat="server" for="pass">Password: </label><asp:TextBox runat="server" ID="pass" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator runat="server" ControlToValidate="pass" ErrorMessage="Password must be atleast 5 characters" ValidationExpression="^[\s\S]{5,}$" ></asp:RegularExpressionValidator>

            <br />
            <label runat="server" for="passConfirm">Confirm Password: </label><asp:TextBox runat="server" ID="passConfirm" TextMode="Password"></asp:TextBox>
                <asp:CompareValidator runat="server" ControlToCompare="pass" ControlToValidate="passConfirm" ErrorMessage="Passwords do NOT match!"></asp:CompareValidator>
                
            </div>

            <label runat="server" for="iconSelect">Choose an icon for your Group:</label>
            <br />
                <asp:FileUpload runat="server" ID="iconSelect" />
                <asp:RegularExpressionValidator runat="server" ControlToValidate="iconSelect" ID="imageCheck" ValidationExpression="^.*\.(png|PNG|jpg|JPG)$" ErrorMessage="Must be jpeg or png file"></asp:RegularExpressionValidator>
            <br />
            <br />

             <label runat="server" for="voiceComms">Voice Communcation Required:</label> <asp:CheckBox runat="server" ID="voiceComms" value="true"/>
            <br />
            <br />
                <asp:ValidationSummary runat="server" DisplayMode="BulletList" />
                <asp:Button runat="server" ID="submit" text="Submit" OnClick="Registration"/>
            <br />
            <br />
            <div id="regDetails" runat="server">
                
            </div>

             <div id="emailConf" runat="server">
                
            </div>
        </div>
    </form>
</body>
</html>
