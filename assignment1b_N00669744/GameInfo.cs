﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment1b_N00669744
{
    public class GameInfo
    {
        public string gameName;
        public int numTeamates;
        public List<string> roles;
        public string platform;
        public bool voiceComms;

        public GameInfo(string gn, int nt, List<string> r,string p, bool vc)
        {
            gameName = gn;
            numTeamates= nt;
            roles = r;
            platform = p;
            voiceComms = vc;
        }
    }
}