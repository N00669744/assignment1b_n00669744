﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment1b_N00669744
{
    public class GroupInfo
    {
        public List<string> daysAvailable;
        public string teamName;
        public bool privateType;
      
        public GroupInfo(List<string> d, string tm, bool grp)
        {
            daysAvailable = d;
            teamName = tm;
            privateType=grp;
        }

    }
}