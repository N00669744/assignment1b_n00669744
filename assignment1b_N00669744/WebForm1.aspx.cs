﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment1b_N00669744
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         
            
        }

        protected void PrivateCheck(object source, ServerValidateEventArgs args)
        {
            if (privateGroup.Checked)
            {
                args.IsValid= true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void Registration(object sender, EventArgs e)
        {
            string teamname = teamName.Text.ToString();
            List<String> days = new List<String> {};
            bool isPrivate = privateGroup.Checked;
            GroupInfo newgroupinfo = new GroupInfo(days,teamname, isPrivate);

            string useremail = emailInput.Text.ToString();
            string password = pass.Text.ToString();
            string passconfirm = passConfirm.Text.ToString();
            UserInfo newuser = new UserInfo(useremail, password, passconfirm);

            string gamename = gameName.Text.ToString();
            int teammates = Int32.Parse(playerNum.SelectedValue.ToString());
            List<String> role = new List<string> { };
            string platform = console.Text.ToString();
            bool voice = voiceComms.Checked;
            GameInfo newgameinfo = new GameInfo(gamename, teammates, role, platform, voice);

            List<string> teamroles = new List<string>();

            foreach(Control roleControl in roles_container.Controls)
            {
                if(roleControl.GetType()==typeof(CheckBox))
                {
                    CheckBox roles = (CheckBox)roleControl;
                    if(roles.Checked)
                    {
                        teamroles.Add(roles.Text);
                    }
                }

            }
            newgameinfo.roles = newgameinfo.roles.Concat(teamroles).ToList();

            List<string> availability = new List<string>();

            foreach (Control daysControl in days_container.Controls)
            {
                if (daysControl.GetType() == typeof(CheckBox))
                {
                    CheckBox day = (CheckBox)daysControl;
                    if (day.Checked)
                    {
                        availability.Add(day.Text);
                    }
                }

            }
            newgroupinfo.daysAvailable = newgroupinfo.daysAvailable.Concat(availability).ToList();
            Registration newgroupcreate = new Registration(newgameinfo, newgroupinfo, newuser);

            regDetails.InnerHtml = newgroupcreate.RegistrationDetails();
            emailConf.InnerHtml = newgroupcreate.EmailConfirmation();
        }
    }
}