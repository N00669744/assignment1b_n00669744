﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment1b_N00669744
{
    public class UserInfo
    {
        private string userEmail;
        private string userPassword;
        private string passConfirm;
 
    public UserInfo(string e, string p, string c)
        {
            userEmail = e;
            userPassword = p;
            passConfirm = c;
        }

    public string UserEmail
        {
            get { return userEmail; }
            set { userEmail=value; }
        }
    private string UserPassword
        {
            get { return userPassword; }
            set { userPassword = value; }
        }
    private string PassConfirm
        {
            get { return passConfirm; }
            set{ passConfirm = value; }
        }
        

    }

}