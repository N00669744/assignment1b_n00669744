﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment1b_N00669744
{
    public class Registration
    {
        public GameInfo gameInfo;
        public GroupInfo groupInfo;
        public UserInfo userInfo;

        public Registration(GameInfo game,GroupInfo group, UserInfo user)
        {
            gameInfo = game;
            groupInfo = group;
            userInfo = user;
        }

        public string RegistrationDetails()
        {
            bool voice = gameInfo.voiceComms;
            bool privateGroup = groupInfo.privateType;
            string regDetails = "Team Name: " + groupInfo.teamName + "<br/><br/>";

            regDetails += "Looking for a group in " + gameInfo.gameName + " for online play on the " + gameInfo.platform + "<br/>";
            if (privateGroup)
            {
                regDetails += "This Group is Private \uD83D\uDD12! Password Required. <br/>";
            }else
            {
                regDetails += "This group is Open \uD83D\uDD13! No Password Required. <br/>";
            }
            regDetails += "Must be available at least one of the following days: \u2611" + String.Join("\u2611 ", groupInfo.daysAvailable.ToArray()) + "<br/>";    
            regDetails += "The team needs " + gameInfo.numTeamates + " player(s) to fill the following role(s): <br/>";
            regDetails += "\u2022"+String.Join("\u2022 ", gameInfo.roles.ToArray())+" " +"<br/>";
            regDetails += "<br/>";
           
            if (voice)
            {
                regDetails += "You MUST have a mic to join! <br/><br/>";
            } else
            {
                regDetails += "You DO NOT need a mic to join! <br/><br/>";
            }
           
            return regDetails;
        }

        public string EmailConfirmation()
        {
            string emailConf = "An email will be sent to " + userInfo.UserEmail+ " for verification. ";
            emailConf += "All details regarding the group (i.e personal messages received, match times, etc." +
                "will be sent there. A Password (if private) will be used by other to gain entry into the group.";

            return emailConf;
        }

    }
}